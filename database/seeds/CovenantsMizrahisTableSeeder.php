<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CovenantsMizrahisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('covenantsmizrahis')->insert(
            [
              [
               'bank_id' => '2',
               'designation' => 'discount',
               'total_month' => '4',
               'max_approval' => '500',
                'approval' => '50',
                'type_check' => 'salaried',
                'created_at' => date('Y-m-d G:i:s'),
               ],
              
            ]); 
    }
}
