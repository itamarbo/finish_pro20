<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payee extends Model
{
    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public function clientdata()
    {
        return $this->hasMany('App\Clientdata');
    }
    protected $fillable = [
        'name', 'adrress', 'occupation','id_account','status', 'created_at', 'updated_at'
    ];
}
