<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public function clientdata()
    {
        return $this->hasMany('App\Clientdata');
    }
    protected $fillable = [
        'client_name', 'adrress', 'occupation','id_account','status', 'created_at', 'updated_at'
    ];
}
